/*
 * 请告诉我所有订单（order）中的订单明细的最大数目、最小数目和平均数目。结果应当包含三列：
 *
 * +────────────────────+────────────────────+────────────────────+
 * | minOrderItemCount  | maxOrderItemCount  | avgOrderItemCount  |
 * +────────────────────+────────────────────+────────────────────+
 */



SELECT MIN(counts) as minOrderItemCount, MAX(counts) as maxOrderItemCount, ROUND(AVG(counts),0) as avgOrderItemCount FROM
(SELECT COUNT(od.orderNumber) as counts
FROM orders o
LEFT JOIN orderdetails od
ON o.orderNumber = od.orderNumber
GROUP BY o.orderNumber) as allorders;