/*
 * 请告诉我所有的订单（`order`）中每一种 `status` 的订单的总金额到底是多少。注意是总金额哦。输出
 * 应当包含如下的信息：
 *
 * +─────────+─────────────+
 * | status  | totalPrice  |
 * +─────────+─────────────+
 *
 * 输出应当根据 `status` 进行排序。
 */

SELECT o.status as status, SUM(orderTotalPrice.summationPrice) as totalPrice
FROM orders o
LEFT JOIN (SELECT orderNumber, SUM(totalPrice) as summationPrice
FROM (SELECT A.orderNumber as orderNumber, A.quantityOrdered*A.priceEach as totalPrice FROM orderdetails A) as stupid GROUP BY orderNumber)
AS orderTotalPrice
ON orderTotalPrice.ordernuMBER = o.orderNumber GROUP BY o.status;


